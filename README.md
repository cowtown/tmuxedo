tmuxedo
=======

My tmux config.

Note the default prefix has been moved to `C-a`.


###Saving/Restoring

`<prefix> C-s` save current session

`<prefix> C-r` restore saved session

####Dependencies

Make sure to install TPM and the tmux-resurrect plugin.

* [TPM](https://github.com/tmux-plugins/tpm) package manager
* [tmux-resurrect](https://github.com/tmux-plugins/tmux-resurrect) - persists tmux env across restarts.


####Misc

* [Cheat Sheet](https://gist.github.com/MohamedAlaa/2961058)

